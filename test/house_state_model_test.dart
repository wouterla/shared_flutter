import 'package:shared_flutter/models/models.dart';
import 'package:test/test.dart';

void main() {
  HouseState houseState1;
  setUp(() {
    houseState1 = new HouseState(isLoading: true, battery: 1, gas: 1, water: 1);
  });
  test('creating HouseState', () {
    expect(1, houseState1.battery);
    expect(true, houseState1.isLoading);
    expect(1, houseState1.gas);
    expect(1, houseState1.water);
    expect(null, houseState1.error);
  });
  test('copy HouseState', () {
    HouseState houseState2 = houseState1.copyWith();
    expect(1, houseState2.battery);
    expect(true, houseState2.isLoading);
    expect(1, houseState2.gas);
    expect(1, houseState2.water);
    expect(null, houseState2.error);
    Exception e = new Exception("test");
    HouseState houseState3 = houseState1.copyWith(
        isLoading: false, water: 3, gas: 3, battery: 3, error: e);
    expect(3, houseState3.battery);
    expect(false, houseState3.isLoading);
    expect(3, houseState3.gas);
    expect(3, houseState3.water);
    expect(e, houseState3.error);
  });
  test('hashCode HouseState', () {
    int code =
        true.hashCode ^ null.hashCode ^ 1.hashCode ^ 1.hashCode ^ 1.hashCode;
    expect(code, houseState1.hashCode);
  });
  test('equals HouseState', () {
    HouseState houseState2 = new HouseState(isLoading: true,battery: 1,water: 1,gas: 1);
    expect(houseState1, houseState2);
    expect(houseState1.copyWith(), houseState1);
  });
  test('toString HouseState', () {
    expect(
        'HouseState{isLoading: true, error: null, battery: 1, water: 1, gas: 1}',
        houseState1.toString());
  });
  test('isLoading HouseState',(){
    expect(true, HouseState.loading().isLoading);
  });
}
