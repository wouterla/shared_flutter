import 'package:shared_flutter/models/models.dart';

class RetrieveHouseStateAction {}

class HouseStateSuccessAction {
  final HouseState houseState;

  HouseStateSuccessAction(this.houseState);
}

class HouseStateError {
  final Exception error;
  HouseStateError(this.error);
}
