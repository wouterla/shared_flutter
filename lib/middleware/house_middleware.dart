import 'dart:async';
import 'package:flutter/services.dart';
import 'package:redux/redux.dart';
import 'package:shared_flutter/actions/actions.dart';
import 'package:shared_flutter/models/models.dart';

void fetchHouseStateMiddleware(
    Store<HouseState> store, action, NextDispatcher next) {
  if (action is RetrieveHouseStateAction) {
    const platform = const MethodChannel('auer.biz.sharedflutter/house');
    Future<Null> _getHouseState() async {
      Map houseState;
      try {
        houseState = await platform.invokeMethod('getHouseState');
        store.dispatch(HouseStateSuccessAction(new HouseState(
            isLoading: false,
            battery: houseState['battery'],
            gas: houseState['gas'],
            water: houseState['water'],
            error: null)));
      } on PlatformException catch (e) {
        store.dispatch(HouseStateError(e));
      }
    }

    _getHouseState();
  }
  next(action);
}
