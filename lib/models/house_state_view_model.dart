import 'package:shared_flutter/models/house_state.dart';

class HouseStateViewModel {
  final HouseState houseState;
  final void Function() onHouseStateRequest;
  HouseStateViewModel({this.houseState, this.onHouseStateRequest});
}
