class HouseState {
  final int battery;
  final int water;
  final int gas;
  final Exception error;
  final bool isLoading;

  HouseState(
      {this.error,
      this.battery = 0,
      this.water = 0,
      this.gas = 0,
      this.isLoading = false});

  factory HouseState.loading() => HouseState(isLoading: true);

  HouseState copyWith(
      {bool isLoading, Exception error, int battery, int water, int gas}) {
    return HouseState(
        isLoading: isLoading ?? this.isLoading,
        battery: battery ?? this.battery,
        water: water ?? this.water,
        gas: gas ?? this.gas,
        error: error ?? this.error);
  }

  @override
  int get hashCode =>
      isLoading.hashCode ^
      error.hashCode ^
      battery.hashCode ^
      water.hashCode ^
      gas.hashCode;

  @override
  operator ==(Object other) =>
      identical(this, other) ||
      other is HouseState &&
          runtimeType == other.runtimeType &&
          isLoading == other.isLoading &&
          error == other.error &&
          battery == other.battery &&
          water == other.water &&
          gas == other.gas;

  @override
  String toString() {
    return 'HouseState{isLoading: $isLoading, error: $error, battery: $battery, water: $water, gas: $gas}';
  }
}
