import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TopInfo extends StatelessWidget {
  final int battery;
  final int gas;
  final int water;

  const TopInfo({Key key, this.battery, this.gas, this.water})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          //new Image.asset('assets/graphics/BMWOneVehicleListCarDefault.png'),
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              new Text('Battery: $battery'),
              new Text("Gas: $gas"),
              new Text('Water: $water')
            ],
          ),
          new Text("Last Updated: 24/7/18 24:59"),
        ]);
  }
}
