import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:shared_flutter/actions/actions.dart';
import 'package:shared_flutter/models/models.dart';
import 'package:shared_flutter/widgets/account_bar.dart';
import 'package:shared_flutter/widgets/actions_bar.dart';
import 'package:shared_flutter/widgets/info_list.dart';
import 'package:shared_flutter/widgets/top_info.dart';

class StartPage extends StatelessWidget {
  final String title;

  const StartPage({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<HouseState, HouseStateViewModel>(
        converter: (store) {
      return new HouseStateViewModel(
          houseState: store.state,
          onHouseStateRequest: () =>
              store.dispatch(RetrieveHouseStateAction()));
    }, builder: (BuildContext context, HouseStateViewModel vm) {
      return new Scaffold(
        appBar: new AppBar(
          title: new Text(title),
        ),
        body: new ListView(
          children: <Widget>[
            new AccountInfo(),
            new TopInfo(
              battery: vm.houseState.battery,
              gas: vm.houseState.gas,
              water: vm.houseState.water,
            ),
            new ActionsBar(viewModel: vm),
            new InfoList()
          ],
        ),
        bottomNavigationBar: new BottomNavigationBar(
          onTap: (int index) {
            switch (index) {
              case 0:
                vm.onHouseStateRequest();
                break;
              case 1:
                vm.onHouseStateRequest();
                break;
              default:
                print(index);
            }
          },
          items: const <BottomNavigationBarItem>[
            const BottomNavigationBarItem(
              icon: const Icon(CupertinoIcons.home),
              title: const Text('Home'),
            ),
            const BottomNavigationBarItem(
              icon: const Icon(CupertinoIcons.battery_charging),
              title: const Text('Overview'),
            ),
            const BottomNavigationBarItem(
              icon: const Icon(CupertinoIcons.book),
              title: const Text('Hub'),
            ),
          ],
        ),
      );
    });
  }
}
