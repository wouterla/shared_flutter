import 'dart:math';

import 'package:flutter/material.dart';
import 'package:shared_flutter/widgets/info_card.dart';

class InfoList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int maxItems = Random.secure().nextInt(10);
    print('Showing $maxItems info cards');
    return new ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) {
          print('At index $index and maxItems is $maxItems');
          if (index < maxItems) {
            return new InfoCard();
          }
        });
  }
}
