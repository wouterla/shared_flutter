import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_flutter/models/models.dart';

class ActionsBar extends StatelessWidget {
  final HouseStateViewModel viewModel;

  const ActionsBar({Key key, this.viewModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          new RaisedButton.icon(
            icon: const Icon(CupertinoIcons.book),
            label: const Text(""),
            onPressed: () {
              viewModel.onHouseStateRequest();
            },
          ),
          new RaisedButton.icon(
            icon: const Icon(CupertinoIcons.flag),
            label: const Text(""),
            onPressed: () {
              viewModel.onHouseStateRequest();
            },
          ),
          new RaisedButton.icon(
            icon: const Icon(CupertinoIcons.home),
            label: const Text(""),
            onPressed: () {
              viewModel.onHouseStateRequest();
            },
          ),
          new RaisedButton.icon(
            icon: const Icon(CupertinoIcons.battery_empty),
            label: const Text(""),
            onPressed: () {
              viewModel.onHouseStateRequest();
            },
          )
        ]);
  }
}
