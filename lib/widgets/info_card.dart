import 'package:flutter/material.dart';

class InfoCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Card(
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: const Icon(Icons.map),
            title: const Text('Title'),
            subtitle: const Text('Subtitle'),
          ),
        ],
      ),
    );
  }
}
