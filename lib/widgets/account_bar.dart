import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AccountInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        const Text("Hello User"),
        new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text("Haus am See"),
          ],
        ),
        new Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [const Icon(CupertinoIcons.right_chevron)])
      ],
    );
  }
}
