import 'package:shared_flutter/actions/actions.dart';
import 'package:shared_flutter/models/models.dart';

HouseState houseStateReducer(HouseState state, action) {
  if (action is RetrieveHouseStateAction) {
    return HouseState.loading();
  } else if (action is HouseStateError) {
    return state.copyWith(isLoading: false, error: action.error);
  } else if (action is HouseStateSuccessAction) {
    return action.houseState;
  } else {
    return null;
  }
}
