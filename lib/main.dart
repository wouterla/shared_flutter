import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:shared_flutter/actions/actions.dart';
import 'package:shared_flutter/models/models.dart';
import 'package:shared_flutter/shared_flutter.dart';
import 'package:shared_flutter/reducers/house_state_reducer.dart';
import 'package:shared_flutter/middleware/house_middleware.dart';

void main() {
  final store = new Store<HouseState>(houseStateReducer,
      initialState: new HouseState(), middleware: [fetchHouseStateMiddleware]);
  store.dispatch(new RetrieveHouseStateAction());
  runApp(new SharedFlutter(store: store));
}
