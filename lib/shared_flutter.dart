import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:shared_flutter/models/models.dart';
import 'package:shared_flutter/widgets/start_page.dart';

class SharedFlutter extends StatelessWidget {
  final Store<HouseState> store;

  const SharedFlutter({Key key, this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreProvider<HouseState>(
        store: store,
        child: new MaterialApp(
          title: 'Shared Flutter Code',
          theme: new ThemeData(
            primarySwatch: Colors.green,
          ),
          home: new StartPage(title: 'Shared Flutter App'),
        ));
  }
}
