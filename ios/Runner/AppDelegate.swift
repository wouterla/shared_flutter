import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController;
    let batteryChannel = FlutterMethodChannel.init(name: "auer.biz.sharedflutter/house",
                                                   binaryMessenger: controller);
    batteryChannel.setMethodCallHandler({
        (call: FlutterMethodCall, result: FlutterResult) -> Void in
        switch call.method {
        case "getHouseState":
            result(self.receiveHouseState())
        case "getBattery":
            result(self.receiveRandomInt())
        case "getWatter":
            result(self.receiveRandomInt())
        case "getGas":
            result(self.receiveRandomInt())
        default:
            result(FlutterMethodNotImplemented);
        }
    });
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    private func receiveBatteryLevel(result: FlutterResult) {
        let device = UIDevice.current;
        device.isBatteryMonitoringEnabled = true;
        if (device.batteryState == UIDeviceBatteryState.unknown) {
            result(FlutterError.init(code: "UNAVAILABLE",
                                     message: "Battery info unavailable",
                                     details: nil));
        } else {
            result(Int(device.batteryLevel * 100));
        }
    }
    
    private func receiveRandomInt() -> Int {
        return Int(arc4random_uniform(101))
    }
    
    private func receiveHouseState() -> NSDictionary {
        let dictionary: NSDictionary = [
            "gas" : receiveRandomInt(),
            "battery" : receiveBatteryLevel(),
            "water" : receiveRandomInt()
        ]
        return dictionary
    }
}
