# Shared Flutter

A shared Flutter application to learn from

## Creation of the App with flutter create

```
flutter create --description "A shared Flutter application to learn from" --org auer.biz -i swift -a kotlin shared_flutter
```

## State Management with Redux

I added [Redux](https://github.com/johnpryan/redux.dart) and [Flutter Redux](https://github.com/brianegan/flutter_redux) for state management in this exercise.

To simulate some connection to the "outside world" I added a native component in both Kotlin and Swift. The middleware connects to it and retrieves some info. The widgets display it and are able to request new state from two different locations.

## To dos:

* [ ] Testing (the only tests available where just to see if it works by default)
* [ ] Navigation between screens. Do you really always need to push a 100% new screen?
* [ ] Trying out the new [WebView widget](https://github.com/dart-flitter/flutter_webview_plugin)
* [ ] Some more complex state than just a few ints (connection to an API would be nice)


## Getting Started

For help getting started with Flutter, view the online
[documentation](https://flutter.io/).
