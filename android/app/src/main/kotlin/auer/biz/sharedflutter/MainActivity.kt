package auer.biz.sharedflutter

import android.os.Bundle

import io.flutter.app.FlutterActivity
import io.flutter.plugins.GeneratedPluginRegistrant
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import java.util.Random

class MainActivity(): FlutterActivity() {

    fun ClosedRange<Int>.random() =
            Random().nextInt((endInclusive + 1) - start) +  start

    private val CHANNEL = "auer.biz.sharedflutter/house"

    override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    GeneratedPluginRegistrant.registerWith(this)
      io.flutter.plugin.common.MethodChannel(flutterView, CHANNEL).setMethodCallHandler { call, result ->
          when (call.method) {
              "getWater" -> result.success(getRandomInt())
              "getBattery" -> result.success(getBatteryLevel())
              "getGas" -> result.success(getRandomInt())
              "getHouseState" -> result.success(getHouseState())
              else -> {
                  result.notImplemented()
              }
      }
    }}

    private fun getBatteryLevel(): Int {
        val batteryLevel: Int
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            val batteryManager = getSystemService(Context.BATTERY_SERVICE) as BatteryManager
            batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
        } else {
            val intent = ContextWrapper(applicationContext).registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
            batteryLevel = intent!!.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) * 100 / intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
        }

        return batteryLevel
    }
        private fun getRandomInt(): Int {
            return (0..100).random()
        }
    private fun getHouseState(): HashMap<String,Int> {
        val hashMap = HashMap<String,Int>(4)
        hashMap["battery"] = getBatteryLevel()
        hashMap["water"] = getRandomInt()
        hashMap["gas"] = getRandomInt()
        return hashMap
    }



}
